# Installation
Before `ROI.plugin.mosek` can be installed `mosek` and its corresponding 
**R** package `Rmosek` need to be installed. 


## Install Rmosek
More information on installing **Rmosek** can be found at [https://www.mosek.com](https://www.mosek.com).


## Install ROI.plugin.mosek

### Version 10

After `mosek` and `Rmosek` are installed simply use
```r
remotes:::install_gitlab("roigrp/solver/ROI.plugin.mosek@mosek10", INSTALL_opts = "--no-multiarch")
```
to install `ROI.plugin.mosek` for MOSEK version 10.


### Version 9.2

`ROI.plugin.mosek` was tested on `Debian GNU/Linux 10` and `Windows 10` with the
`mosek` version `9.2`.      

After `mosek` and `Rmosek` are installed simply use
```r
remotes:::install_gitlab("roigrp/solver/ROI.plugin.mosek@mosek9.2", INSTALL_opts = "--no-multiarch")
```
to install `ROI.plugin.mosek` for MOSEK version 9.2.


### Version 8

After `mosek` and `Rmosek` are installed simply use
```r
remotes:::install_gitlab("roigrp/solver/ROI.plugin.mosek@mosek8", INSTALL_opts = "--no-multiarch")
```
to install `ROI.plugin.mosek` for MOSEK version 8.
